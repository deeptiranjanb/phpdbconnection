<?php
session_start();
require_once("Database.php");
require_once("user.php");

$dbObj = Database::getInstance();

//Username and password validation
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $userObj = new user();

    $name = $userObj->check_input($_POST["name"]);
    $password = $_POST["password"];
    $cpassword = $_POST["cpassword"];


    if ($userObj->emptyCheck($name, $password) === -1) {
        $err = "username or password cannot be empty";
    } else {
        $is_valid = $userObj->register($name, $password, $cpassword, $dbObj);

        if ($is_valid === -2) {
            $err = "passwords don't match";
        } elseif ($is_valid === 2) {
            header("location:successful.php");
        } elseif ($is_valid === -1) {
            $err = "user already exists";
        } else {
            $err = "registration could not be completed";
        }
    }
}


?>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name='author' content='Deeptiranjan'>
    <title>Sign Up</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-1/css/all.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/login_style.css?v=1">
    <link rel="stylesheet" type="text/css" href="assets/profile_style.css?v=1">
    <link rel="stylesheet" type="text/css" href="assets/footer_style.css">
</head>

<body class='bg-light'>
    <div class="container">
        <div class="row">
            <div class="col-md-12  d-flex flex-column justify-content-center">
                <div class="row">
                    <div class="col-lg-6 col-md-8 mx-auto">

                        <!-- form card login -->
                        <div class="card rounded shadow shadow-sm">
                            <div class="card-header bg-dark">
                                <h3 class="mb-0 text-white">Sign Up</h3>
                            </div>
                            <div class="card-body">
                                <form class="form" role="form" autocomplete="off" id="formLogin" method="POST">
                                    <div class="form-group">
                                        <label for="uname1">Enter Username</label>
                                        <input type="text" class="form-control form-control-lg rounded-0" name="name" id="uname1" value="<?php echo $name ?>">
                                        <span class="error"><?php echo $nameErr; ?></span>
                                    </div>
                                    <div class="form-group">
                                        <label>Enter Password</label>
                                        <input type="password" name="password" class="form-control form-control-lg rounded-0" id="pwd1">
                                        <span class="error"><?php echo $passwordErr; ?></span>
                                    </div>
                                    <div class="form-group">
                                        <label>Confirm Password</label>
                                        <input type="password" name="cpassword" class="form-control form-control-lg rounded-0" id="cpwd1">
                                        <span class="error"><?php echo $passwordErr; ?></span>
                                    </div>
                                    <span class="error"> <?php echo $err; ?> </span>

                                    <button type="submit" class="btn btn-success btn-block btn-lg float-right" id="btnLogin">SIGN UP</button>


                                </form>
                            </div>
                            <!--/card-block-->
                        </div>
                        <!-- /form card login -->

                    </div>


                </div>
                <!--/row-->

            </div>
            <!--/col-->
        </div>
        <!--/row-->
    </div>
    <!--/container-->

    <?php require_once("footer.php"); ?>

</body>

</html>