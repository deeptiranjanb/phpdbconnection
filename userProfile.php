<?php
require_once("Database.php");
class userProfile
{
    private $dbObj;
    private $dbConn;
    private $errCount = 0;
    public function validate($userDetails, $dbObj, $dbConn)
    {
        $this->dbObj = $dbObj;
        $this->dbConn = $dbConn;


        //name validation
        if (empty($userDetails["name"])) {
            $nameErr = "Please enter your name";
            $this->errCount++;
        } else {
            $name = ($userDetails["name"]);
            if (!preg_match("/^[a-zA-Z ]*$/", $name)) {
                $nameErr = "Only letters and white space allowed";
                $this->errCount++;
            }
        }
        //Email validation
        if (empty($userDetails["email"])) {
            $emailErr = "Please enter your email";
            $this->errCount++;
        } else {
            $email = ($userDetails["email"]);
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $emailErr = "Please enter valid email";
                $this->errCount++;
            }
        }
        //Existing user check
        $conditions = array(
            "where" => array(
                "name" => $userDetails['name'],
                "email" => $userDetails['email'],
            ),
            "operators" => ["AND"]
        );
        $tableName = "profile_details";
        $result = $this->dbObj->select($tableName, $conditions);
        if (count($result) != 0) {
            $this->errCount++;
            $userErr = "User already exists";
        }

        //mobile number validation
        if (empty($userDetails["mob"])) {
            $mobErr = "Please enter your mobile";
            $this->errCount++;
        } else {
            $mob = ($userDetails["mob"]);
            if ((is_numeric($mob) and strlen($mob) == 10)) {
            } else {
                $mobErr = "Please enter valid mobile number";
                $this->errCount++;
            }
        }
        //age validation
        if (empty($userDetails["age"])) {
            $ageErr = "Please enter your age";
            $this->errCount++;
        } else {
            $age = ($userDetails["age"]);
            if ($age < 20 or $age > 30) {
                $ageErr = "Valid age is between 20-30";
                $this->errCount++;
            }
        }
        //gender validation
        if (empty($userDetails["gender"])) {
            $genderErr = "Please select a gender";
            $this->errCount++;
        } else {
            $gender = ($userDetails["gender"]);
        }
        //state validation
        if (empty($userDetails["state"])) {
            $stateErr = "Please select a state";
            $this->errCount++;
        } else {
            $state = ($userDetails["state"]);
        }
        //skills validation
        if (count($userDetails["skill"]) < 2) {
            $skillErr = "Please select at least two skill";
            $this->errCount++;
        } else {
            foreach ($userDetails['skill'] as $skills) {
                $skill[] = ($skills);
            }
        }
        //profile photo validation
        $photoTempName = $userDetails["profilePhoto"]["tmp_name"];
        $photoType = mime_content_type($photoTempName);
        $photoName = $userDetails["profilePhoto"]["name"];
        $formatArray = ['jpg', 'png', 'jpeg'];
        $index = strpos($photoType, '/');
        $photoFormat = substr($photoType, $index + 1);
        //  $photoName = 'profilepic' . time() . '.' . $photoFormat;
        $fileStore = "uploads/" . $photoName;
        if (in_array($photoFormat, $formatArray) and $userDetails["profilePhoto"]["size"] < 1001718) {
            if (move_uploaded_file($photoTempName, $fileStore)) {
                $photo_status =  "File is valid, and was successfully uploaded";
            } else {
                $this->errCount++;
                $profilePhotoErr = "photo is not uploaded";
            }
        } else {
            $profilePhotoErr = "photo size or format is not supported.";
        }
        //resume validation
        $resumeTempName = $userDetails["resume"]["tmp_name"];
        $resumeName = $userDetails["resume"]["name"];
        $resumeType = mime_content_type($resumeTempName);
        $index = strpos($resumeType, '/');
        $resumeFormat = substr($resumeType, $index + 1);
        //  $resumeName = 'resume' . time() . '.' . $resumeFormat;
        $resumeStore = "uploads/" . $resumeName;
        if ($resumeFormat == 'pdf' and $userDetails["resume"]["size"] < (1001718 * 2)) {
            if (move_uploaded_file($resumeTempName, $resumeStore)) {
                $resumeStatus =  "File is valid, and was successfully uploaded";
            } else {
                $this->errCount++;
                $resumeErr = "resume is not uploaded";
            }
        } else {
            $resumeErr = "Resume size or format is not supported.";
        }
        $err = $this->errCount;
        return compact("err", "userErr", "nameErr", "emailErr", "genderErr", "stateErr", "skillErr", "mobErr", "ageErr", "profilePhotoErr", "resumeErr", "resumeName", "photoName");
    }
    public function register($userDetails, $dbObj, $dbConn)
    {
        $this->dbObj = $dbObj;
        $this->dbConn = $dbConn;

        $tableName = "profile_details";
        $columns = array(
            "name" => $userDetails["name"],
            "email" => $userDetails["email"],
            "mobileNO" => $userDetails["mob"],
            "age" => $userDetails["age"],
            "state" => $userDetails["state"],
            "gender" => $userDetails["gender"],
            "photoName" => $userDetails["photoName"],
            "resumeName" => $userDetails["resumeName"]
        );
        try {

            if ($this->dbObj->insert($tableName, $columns)) {
                $err = 1;
            } else {
                $err = "data could not be inserted";
            }
        } catch (Exception $e) {
            $err = "error in profile_details insert; " . $e->getMessage();
        }
        try {
            $userId = $this->dbConn->lastInsertId();
        } catch (Exception $e) {
            $err = "error in getting userId: " . $e->getMessage();
        }

        foreach ($userDetails['skill'] as $sk) {
            try {
                $conditions = array(
                    "select" => ["id"],
                    "where" => array(
                        "skillName" => $sk
                    )
                );
                $tableName = "skills";
                $result = $this->dbObj->select($tableName, $conditions);
            } catch (Exception $e) {
                $err = "Error in in getting skills id: " . $e->getMessage();
            }
            try {
                if (count($result) > 0) {
                    $skillId = $result[0]["id"];
                    $tableName = "user_skills";
                    $columns = array(
                        "userId" => $userId,
                        "skillsId" => $skillId
                    );
                    if ($this->dbObj->insert($tableName, $columns) === TRUE) {
                        $err = 1;
                    } else {
                        $err = "data could not be entered";
                    }
                }
            } catch (Exception $e) {
                $err =  "Eroor in entering to user_skills: " . $e->getMessage();
            }
        }
        return $err;
    }

    public function display($userDetails, $dbObj, $dbConn)
    {
        $conditions = array(
            "select" => ["id", "name", "email", "mobileNO", "age", "state", "gender", "photoName", "resumeName"],
            "where" => array(
                "name" => $userDetails["name"]
            )
        );
        $tableName = "profile_details";
        try {
            $result = $this->dbObj->select($tableName, $conditions);
        } catch (Exception $e) {
            $err = "error in getting profile details" . $e->getMessage();
        }

        $details = $result[0];
        $conditions = array(
            "select" => ["skillName"],
            "where" => array(
                "skills.id" => "user_skills.skillsId",
                "profile_details.id" => "user_skills . userId",
                "profile_details.name" => "'" . $userDetails["name"] . "'"
            ),
            "operators" => ["AND", "AND"]
        );
        $tableName = ["profile_details", "user_skills", "skills"];
        try {
            $result = $this->dbObj->select($tableName, $conditions);
        } catch (Exception $e) {
            $err = "error in selecting skill" . $e->getMessage();
        }
        $skills = [];
        if (count($result) > 0) {
            foreach ($result as $key => $value) {
                array_push($skills, $value["skillName"]);
            }
        }
        $details["skills"] = $skills;
        $details["err"] = $err;
        return $details;
    }
}
