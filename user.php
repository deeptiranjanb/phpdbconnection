<?php
require_once("Database.php");

class user
{
    private $name;
    private $password;
    private $obj;

    public function emptyCheck($username, $password)
    {
        $this->name = $username;
        $this->password = $password;

        if (empty($this->name) || empty($this->password)) {
            return -1;
        }
    }

    public function validate($name, $password, $obj)
    {
        $this->name = $name;
        $this->password = $password;
        $this->obj = $obj;

        $password_hash = hash('sha256', $this->password);
        $conditions = array(
            "select" => ["userName", "password"],
            "where" => array(
                "userName" => $this->name,
                "password" => $password_hash
            ),
            "operators" => ["AND"]
        );
        $tableName = "users";

        $result = $this->obj->select($tableName, $conditions);

        if (count($result) === 1) {
            return 1;
        }
        return 0;
    }

    public function register($name, $password, $cpassword, $dbObj)
    {
        $this->name = $name;
        $this->password = $password;
        $this->obj = $dbObj;

        if ($this->password != $cpassword) {
            return -2;
        }

        $password_hash = hash('sha256', $this->password);
        $conditions = array(
            "select" => ["id"],
            "where" => array(
                "userName" => $this->name
            )
        );
        $tableName = "users";
        $result = $this->obj->select($tableName, $conditions);

        if (count($result) == 0) {
            $columns = array(
                "userName" => $this->name,
                "password" => $password_hash
            );
            $tableName = "users";
            if ($this->obj->insert($tableName, $columns)) {
                return 2;
            }
            return 0;
        } else {
            return -1;
        }
    }
    public function check_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);

        return $data;
    }
}
