<?php
class Database
{
    private static $connection = null;
    private $db;
    private $dbHost = "localhost";
    private $dbUsername = "root";
    private $dbPassword = "780952@Drb";
    private $dbName = "PhpDb";

    private function __construct()
    {
        try {
            $conn = new PDO("mysql:host=" . $this->dbHost . ";dbname=" . $this->dbName, $this->dbUsername, $this->dbPassword);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->db = $conn;
        } catch (PDOException $e) {
            die("Failed to connect with MySQL: " . $e->getMessage());
        }
    }

    public static function getInstance()
    {
        if (self::$connection == null) {
            self::$connection = new Database();
        }

        return self::$connection;
    }

    public function getConnection()
    {
        return $this->db;
    }
    /* 
    ***********************************************
    input format for select
    ***********************************************
          $conditions = array(
            "select" => ["userName", "password"],
            "join" => [,,]
            "where" => array(
                "userId" => "10",
                "userName" => "20",
                "password" => "1234"
            ),
            "operators" => ["AND", "OR"]
        );
        $tableName = "users";
  **tableName should be an array for join operation**
    */

    public function select($tableName, $conditions)
    {

        $sql = "SELECT ";
        $sql .= array_key_exists("select", $conditions) ? implode(', ', $conditions["select"]) : '*';
        $sql .= " FROM ";

        if (is_array($tableName)) {
            $sql .= implode(" join ", $tableName);
            if (array_key_existS("where", $conditions)) {
                $sql .= " WHERE ";
                $i = 0;
                foreach ($conditions["where"] as $keys => $values) {
                    $sql .= $keys . " = " . $values;
                    if (array_key_exists("operators", $conditions) and $i < count($conditions["operators"])) {
                        $sql .= ' ' . $conditions["operators"][$i] . ' ';
                        $i++;
                    }
                }

                $statement = $this->db->prepare($sql);
                $statement->execute();
                return $statement->fetchAll();
            }
        } else {
            $sql .= $tableName;

            if (array_key_existS("where", $conditions)) {
                $sql .= " WHERE ";
                $i = 0;

                foreach (array_keys($conditions["where"]) as $keys) {
                    $sql .= $keys;
                    $sql .= " = :" . $keys;

                    if (array_key_exists("operators", $conditions) and $i < count($conditions["operators"])) {
                        $sql .= ' ' . $conditions["operators"][$i] . ' ';
                        $i++;
                    }
                }
            }
            $statement = $this->db->prepare($sql);
            if (array_key_exists("where", $conditions)) {

                $statement->execute($conditions["where"]);
                return $statement->fetchAll();
            }
            $statement->execute();
            return $statement->fetchAll();
        }
    }
    /* 
    ***********************************************
    input format for insert
    ***********************************************
        $tableName = "profile_details";
        $columns = array(
            "name" => $userDetails["name"],
            "email" => $userDetails["email"],
            "mobileNO" => $userDetails["mob"],
            "age" => $userDetails["age"],
            "state" => $userDetails["state"],
            "gender" => $userDetails["gender"],
            "photoName" => $userDetails["photoName"],
            "resumeName" => $userDetails["resumeName"]
        );
    */
    public function insert($tableName, $columns)
    {
        // print_r($columns);
        $sql = "INSERT INTO " . $tableName . "(" . implode(",", array_keys($columns)) . ") VALUES (:" . implode(", :", array_keys($columns)) . ")";
        $statement = $this->db->prepare($sql);
        return $statement->execute($columns);
    }
}
