<?php
session_start();
if (!isset($_SESSION["name"])) {
    header("location:index.php");
}
require_once("userProfile.php");
require_once("Database.php");
require_once("indian_state.php");

$dbObj = Database::getInstance();
$dbConn = $dbObj->getConnection();


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $userDetails = array(
        "name" => $_POST["name"],
        "email" => $_POST["email"],
        "mob" => $_POST["mob"],
        "age" => $_POST["age"],
        "gender" => $_POST["gender"],
        "state" => $_POST["state"],
        "skill" => $_POST["skill"],
        "profilePhoto" => $_FILES["profilePhoto"],
        "resume" => $_FILES["resume"]
    );

    $userProfileObj = new userProfile();
    $errorMessage = $userProfileObj->validate($userDetails, $dbObj, $dbConn);

    if ($errorMessage['err'] == 0) {
        $userDetails["photoName"] = $errorMessage["photoName"];
        $userDetails["resumeName"] = $errorMessage["resumeName"];
        $dbError =  $userProfileObj->register($userDetails, $dbObj, $dbConn);
    }

    if ($dbError == 1) {
        $details =    $userProfileObj->display($userDetails, $obj, $sql);
    } else {
        $dbError = "profile could not be displayed";
    }
}
?>
<!DOCTYPE HTML>
<html>

<head>
    <meta name='author' content='Deeptiranjan'>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Profile Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-1/css/all.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/profile_style.css?v=1">
    <link rel="stylesheet" type="text/css" href="assets/footer_style.css">
</head>
<?php require_once('navbar.php'); ?>

<body class='bg-light'>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5">

                <form method="post" class='inputForm' action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" enctype='multipart/form-data'>
                    <div class='form-group'>
                        <label for="name">Name:</label>
                        <input type="text" name="name" class="form-control" value="<?php echo $userDetails['name']; ?>">
                        <span class="error"><?php echo $errorMessage['nameErr']; ?></span>
                    </div>
                    <div class='form-group'>
                        <label for="email">E-mail:</label>
                        <input type="email" name="email" class="form-control" value="<?php echo $userDetails['email']; ?>">
                        <span class="error"><?php echo $errorMessage['emailErr']; ?></span>
                    </div>
                    <div class='form-group'>
                        <label for="mobile no">Mobile no:</label>
                        <input type="text" name="mob" class="form-control" value="<?php echo $userDetails['mob']; ?>" pattern="[1-9]{1}[0-9]{9}" maxlength="10">
                        <span class="error"><?php echo $errorMessage['mobErr']; ?></span>
                    </div>
                    <div class='form-group'>
                        <label for="age">Age:</label>
                        <input type="text" name="age" class="form-control" value="<?php echo $userDetails['age']; ?>">
                        <span class="error"><?php echo $errorMessage['ageErr']; ?></span>
                    </div>
                    <div class='custom-control-inline'>
                        <label class='d-block' for="gender">Gender:</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="female" name="gender" value="female">
                        <label class="custom-control-label" for="female">Female</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="male" name="gender" value="male">
                        <label class="custom-control-label" for="male">Male</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="others" name="gender" value="others">
                        <label class="custom-control-label" for="others">Others</label>
                    </div>
                    <div class="form-group">
                        <span class="error"><?php echo $errorMessage['genderErr']; ?></span>
                    </div>
                    <div class='form-group'>
                        <label class='d-block' for="state">State:</label>
                        <select name="state" id="state" class="custom-select">
                            <option value="">Select any state</option>
                            <?php
                            foreach ($states as $name) {
                                var_dump($name); ?>
                                <option value="<?php echo $name ?>"><?php echo $name ?></option>
                            <?php
                            }
                            ?>
                        </select></p>
                    </div>
                    <span class="error"><?php echo $errorMessage['stateErr']; ?></span>
                    <div class='form-group  '>
                        <label class='d-block' for="skills">Skills:</label>
                        <div class="custom-control custom-checkbox custom-control-inline ">
                            <input type='checkbox' name='skill[]' class="custom-control-input" id="c" value='c'>
                            <label class="custom-control-label" for="c">C</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline ">
                            <input type='checkbox' name='skill[]' class="custom-control-input" id="java" value='java'>
                            <label class="custom-control-label" for="java">Java</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline ">
                            <input type='checkbox' name='skill[]' class="custom-control-input" id="python" value='python'>
                            <label class="custom-control-label" for="python">Python</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline ">
                            <input type='checkbox' name='skill[]' class="custom-control-input" id="web_development" value='web development'>
                            <label class="custom-control-label" for="web_development">Web Development</label>
                        </div>

                    </div>
                    <span class="error"><?php echo $errorMessage['skillErr']; ?></span>

                    <div class="custom-file button container-fluid">
                        <input type="file" class="custom-file-input" id="profilephoto" name='profilePhoto'>
                        <label class=" custom-file-label" for="profilephoto">Choose profile photo</label>

                    </div>
                    <span class="error"><?php echo $errorMessage['profilePhotoErr']; ?></span>
                    <div class="custom-file button container-fluid">
                        <input type="file" class="custom-file-input col-md-3" name='resume' id="resume">
                        <label class="custom-file-label" for="resume">Choose resume</label>
                    </div>
                    <span class="error"><?php echo $errorMessage['resumeErr']; ?></span>
                    <span class="error"><?php echo $errorMessage['userErr']; ?></span>
                    <div class='button'>
                        <input type="submit" class="btn btn-block btn-primary" name="submit" value="Submit">
                    </div>
                </form>
            </div>
            <div class="col-md-7">
                <?php
                //Details entered printed in a table
                if ($_POST['submit'] != "") { ?>
                    <table class="table table-bordered table-hover">
                        <caption><?php echo $details["err"]; ?></caption>
                        <tr>
                            <th>
                                ProfilePhoto:
                            </th>
                            <td>
                                <img src=<?php echo 'uploads/' . $details['photoName']; ?> alt='profile photo' width="100px">

                            </td>
                        </tr>
                        <tr>
                            <th>
                                Name:
                            </th>
                            <td>
                                <?php echo $details['name']; ?>

                            </td>
                        </tr>
                        <tr>
                            <th>
                                Email:
                            </th>
                            <td>
                                <?php echo $details['email']; ?>

                            </td>
                        </tr>
                        <tr>
                            <th>
                                Mobile no:
                            </th>
                            <td>
                                <?php echo $details['mobileNO']; ?>

                            </td>
                        </tr>
                        <tr>
                            <th>
                                Age:
                            </th>
                            <td>
                                <?php echo $details['age']; ?>

                            </td>
                        </tr>
                        <tr>
                            <th>
                                Gender:
                            </th>
                            <td>
                                <?php echo $details['gender']; ?>

                            </td>
                        </tr>
                        <tr>
                            <th>
                                State:
                            </th>
                            <td>
                                <?php echo $details['state']; ?>

                            </td>
                        </tr>
                        <tr>
                            <th>
                                Skills:
                            </th>
                            <td>
                                <?php echo implode(', ', $details['skills']); ?>

                            </td>
                        </tr>


                        <tr>
                            <th>
                                Resume:
                            </th>
                            <td>
                                <a href="<?php echo 'uploads/' . $details['resumeName']; ?>" download> Download resume</a>
                            </td>
                        </tr>
                    </table> <?php } ?>
            </div>
        </div>
    </div>
    </div>

    <?php require_once('footer.php') ?>

</body>


</html>